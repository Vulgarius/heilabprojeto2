﻿using UnityEngine;

public class LookAtBehaviour : MonoBehaviour
{
    //The LookAt target's transform
    public Transform Target;

	void Update ()
    {
        //General LookAt
        transform.LookAt(Target.position);

        //Lock LookAt to one axis by grounding the other two
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
	}
}
