﻿using System.Collections;
using UnityEngine;

public class ActivateObjectAfterTime : MonoBehaviour
{
    //The time to wait
    public float TimeToWait;

    //The object to activate
    public GameObject ObjectToActivate;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ActivateObject());
    }

    private IEnumerator ActivateObject()
    {
        //Wait x time
        yield return new WaitForSecondsRealtime(TimeToWait);

        //Activate the target object
        ObjectToActivate.SetActive(true);
    }
}
