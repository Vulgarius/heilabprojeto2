﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gotoaftertest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadingtheResultsScreen()
    {
        Debug.Log("All questions have been answered! Loading last scene...");
        //Load the last scene
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1);
    }
}
