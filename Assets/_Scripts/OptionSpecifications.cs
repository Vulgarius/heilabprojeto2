﻿using UnityEngine;
using UnityEngine.UI;

/*
 * 
 * The evaluation for each answer.
 * 
 */

public enum AnswerEvaluation
{
    //The "selfish" answer
    Bad,
    //The answer that falls in between good and bad
    Neutral,
    //The altruistic answer
    Good
}

/*
 * 
 * This script, beyond storing the evaluation for this answer,
 * also replaces the button's text with the answer text.
 * As such, if an answer's text needs to be changed, it should
 * be changed in that button's instance of this script and not
 * in the Text component.
 * 
 */

public class OptionSpecifications : MonoBehaviour
{
    //The answer's text
    public string AnswerText;

    //The answer's evaluation
    public AnswerEvaluation AnswerEval;

    // Start is called before the first frame update
    void Start()
    {
        //Get this answer's text and replace the button's text with it
        GetComponentInChildren<Text>().text = AnswerText;
    }
}
