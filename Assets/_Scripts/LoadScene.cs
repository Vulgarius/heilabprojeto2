﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * 
 * This script, in addition to loading a scene by situation
 * number, stores in the AnswerHolder that an answer to a
 * situation was given, along with its evaluation.
 * 
 * It also plays reaction animations intended to demonstrate
 * the aftermath of a decision, whether it is the other
 * character's reaction, the user character's movement, or
 * both.
 * To do this, it needs to contain (in the ReactionAnimators
 * array) all the characters and/or environment elements
 * that will react to the answer.
 * 
 */

public class LoadScene : MonoBehaviour
{
    //All the objects that will be animated in reaction to the user's answer
    public Animator[] ReactionAnimators;

    //The time waited until the main menu is loaded after answering (depends on reaction animation)
    public float TimeUntilLoadMainMenu = 4f;

    //The situation's options
    private Button[] _optionButtons = new Button[3];

    private void Awake()
    {
        //If this scene is a situation
        if (SceneManager.GetActiveScene().name.Contains("Situation"))
        {
            //Get its buttons
            for(int i = 0; i < 3; i++)
            {
                _optionButtons[i] = GameObject.FindGameObjectWithTag("Button" + (i + 1).ToString()).GetComponent<Button>();
            }
        }
    }

    public void ConfirmChoice(OptionSpecifications choice)
    {
        //Deactivate interaction in all buttons
        foreach(Button option in _optionButtons)
        {
            option.interactable = false;
        }

        //This switch case is pretty much here in case there
        //is the need to store the answer evaluations in a file
        switch(choice.AnswerEval)
        {
            //If the answer was bad
            case AnswerEvaluation.Bad:
                //Register the answer as bad (if necessary) and play the corresponding animation
                Debug.Log("Bad choice...");
                PlayReactionStates("BadReaction");
                break;
            //If the answer was neutral
            case AnswerEvaluation.Neutral:
                //Register the answer as neutral (if necessary) and play the corresponding animation
                Debug.Log("Could be better.");
                PlayReactionStates("NeutralReaction");
                break;
            //If the answer was good
            case AnswerEvaluation.Good:
                //Register the answer as good (if necessary) and play the corresponding animation
                Debug.Log("Great choice!");
                PlayReactionStates("GoodReaction");
                break;
        }

        //Set this situation as answered
        GameObject.FindGameObjectWithTag("AnswerHolder").GetComponent<AnswerHolder>().SituationSolved[SceneManager.GetActiveScene().buildIndex - 2] = true;
        Debug.Log(SceneManager.GetActiveScene().buildIndex - 3);
        //Pass its answer evaluation
        GameObject.FindGameObjectWithTag("AnswerHolder").GetComponent<AnswerHolder>().Answers[SceneManager.GetActiveScene().buildIndex - 2] = choice.AnswerEval;

        Debug.Log("Answered situation " + (SceneManager.GetActiveScene().buildIndex - 2).ToString());

        StartCoroutine(LoadMenu());
    }

    protected IEnumerator LoadMenu()
    {
        yield return new WaitForSecondsRealtime(TimeUntilLoadMainMenu);

        //Load the situation selection menu
        SceneManager.LoadScene(1);
    }

    protected void LoadNextScene()
    {
        //Load the scene with the next build index
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadSituationByIndex(int situationIndex)
    {
        //Load the scene by situation index (adjusted so that it takes in the situation number)
        GameObject.FindGameObjectWithTag("AnswerHolder").GetComponent<AnswerHolder>().GoToScene(situationIndex);
    }

    private void PlayReactionStates(string stateName)
    {
        /*
         * 
         * Play the indicated Animator state
         * 
         * In order to maintain this code functional, use the following names for each reaction state:
         * 
         * Reaction to a bad answer     -   "BadReaction"
         * Reaction to a neutral answer -   "NeutralReaction"
         * Reaction to a good answer    -   "GoodReaction"
         * 
         */

        //Goes through each animator and plays the animation intended
        foreach(Animator anim in ReactionAnimators)
        {
            anim.Play(stateName);
        }
    }
}
