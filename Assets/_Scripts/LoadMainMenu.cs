﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMainMenu : MonoBehaviour
{
    public void LoadMenu()
    {
        //Load the situation selection scene
        SceneManager.LoadScene(1);
    }
}