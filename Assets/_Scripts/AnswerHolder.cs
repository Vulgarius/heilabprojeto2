﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * 
 * This script holds the user's given answers.
 * At each start, it should reset the answers,
 * so it checks if it should reset the answer bools
 * whenever needed.
 * 
 * For each situation, there is a boolean that tells if it
 * was answered, and a corresponding AnswerEvaluation to
 * tell if the answer was Good, Neutral or Bad.
 * 
 * Whenever the situation selection menu is loaded, this
 * script checks if all answers were given and loads the
 * last scene (listed in the Build Settings) if they were.
 * 
 */

public class AnswerHolder : MonoBehaviour
{
    //The selection button's parent
    private Transform _buttonParent;

    //Check if the bools should be reset (allows multiple tests without restarting)
    private bool _shouldResetBools = true;

    //Check if each situation has been answered
    public bool[] SituationSolved = new bool[6];

    //Store the answer evaluation for each situation
    public AnswerEvaluation[] Answers = new AnswerEvaluation[6];

    private void Awake()
    {
        //Don't destroy this object between scenes
        DontDestroyOnLoad(gameObject);

        //Check if the situation bools have to be reset
        if (_shouldResetBools)
        {
            //Reset the bools
            ResetBools();

            //Set the bools as reset
            _shouldResetBools = false;
        }

        //Whenever a scene is loaded, the CheckButton event will be triggered
        SceneManager.sceneLoaded += CheckButton;
    }

    private void ResetBools()
    {
        //For each situation bool
        for (int i = 0; i < 5; i++)
        {
            //Set the situation as unanswered
            SituationSolved[i] = false;
        }
    }

    private void CheckButton(Scene scene, LoadSceneMode loadSceneMode)
    {
        GameObject resultobject = GameObject.FindGameObjectWithTag("Totheresults");
        //If the scene is the selection menu
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            //                                                                  button----image--parent
            _buttonParent = GameObject.FindGameObjectWithTag("SelectionButton").transform.parent.parent;

            //Create a new GameObject list that will contain the situation buttons
            List<GameObject> selectionButtons = new List<GameObject>();

            //Add each situation button to the list
            for (int i = 0; i < 5; i++)
            {
                selectionButtons.Add(_buttonParent.GetChild(i).GetChild(0).gameObject);
            }

            //Count how many buttons are inactive
            int deactivationCount = 0;

            //If the situation was answered, set its interaction as inactive
            for (int i = 0; i < selectionButtons.Count; i++)
            {
                // Para não voltar atrás
                //selectionButtons[i].GetComponent<Button>().interactable = !SituationSolved[i];

                //If the situation was answered
                if(SituationSolved[i])
                {
                    //Increase the answer count
                    deactivationCount++;
                }
            }
            int howmanyanswered = 0;
            foreach(bool situationsolved in SituationSolved)
            {
                if (situationsolved)
                {
                    howmanyanswered++;
                }
            }
            //If every situation was answered
            if(howmanyanswered == 5)
            {
                resultobject.GetComponent<Button>().interactable = true;
                
                
            }
            /*if(deactivationCount == 5)
            {
                //Load the results screen
                LoadResultsScreen();
            }*/
        }
    }

    public void GoToScene(int sceneIndex)
    {
        //Load the scene
        SceneManager.LoadScene("Situation" + sceneIndex.ToString());
        //SceneManager.LoadScene(sceneIndex + 3);
    }

    private void LoadResultsScreen()
    {
        Debug.Log("All questions have been answered! Loading last scene...");
        //Load the last scene
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1);
    }
}
