﻿using UnityEngine;
using UnityEngine.UI;

/*
 * 
 * This script checks if the answers given were correct
 * in order to activate/deactivate the buttons that allow
 * the user to check the correct answer.
 * 
 * Correct answer = button not interactable
 * Incorrect answer = button interactable
 * 
 * The deactivation of button interactivity based on the
 * answer given is functional; there is still no way to
 * check the correct answers, there needs to be a pop-up
 * with the correct answer to the situation selected.
 * 
 */

public class AnswerChecker : MonoBehaviour
{
    //The script that holds the answers and their evaluations
    private AnswerHolder _answers;

    //The buttons that pop the correct answers up
    private Button[] _answerButtons = new Button[5];

    void Awake()
    {
        //Instance creation
        _answers = GameObject.FindGameObjectWithTag("AnswerHolder").GetComponent<AnswerHolder>();
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("SelectionAnswerCheck");
        for (int i = 0; i < buttons.Length; i++)
        {
            _answerButtons[i] = buttons[i].GetComponent<Button>();
        }
    }

    private void Start()
    {
        for(int i = 0; i < _answerButtons.Length; i++)
        {
            //Toggle button interactivity according to the answer given to the corresponding situation
            _answerButtons[i].interactable = CheckIfShouldShowAnswer(_answers.Answers[i]);
        }
    }

    private bool CheckIfShouldShowAnswer(AnswerEvaluation answerEvaluation)
    {
        //If the answer was incorrect (Bad or Neutral), show the answer
        //If the answer was correct (Good), don't show the answer
        return answerEvaluation != AnswerEvaluation.Good;
    }
}
